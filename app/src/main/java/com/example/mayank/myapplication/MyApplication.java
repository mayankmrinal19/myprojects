package com.example.mayank.myapplication;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

/**
 * Created by mayank on 5/11/16.
 */

public class MyApplication extends Application {
    private static final boolean isProduction       = false;
    private static final boolean isInternalRelease  = true;
    private static final String TAG = MyApplication.class.getName();

    public static Context context;

    // Other:
    private Preferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        context = getApplicationContext();

//        preferences = new Preferences(this);

        // Initialize the Facebook SDK:
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
//        if (isProduction) {
//            APIConfigurator.getInstance(getApplicationContext(), true);
//        }
//        else {
//            APIConfigurator.getInstance(getApplicationContext(), false);
//        }
//
//        StudentAppDBManager.getInstance(getApplicationContext());
//
//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
//                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//            TringSDK.initWithLog(isInternalRelease, getApplicationContext());
//        }
    }

    @Override
    public void onTerminate() {
        Log.d(TAG, "onTerminate()");
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

        }
        super.onTerminate();
    }

    public Preferences getPreferences() {
        synchronized (this) {
            return preferences;
        }
    }

    public static MyApplication getInstance(Context context) {
        return (MyApplication) context.getApplicationContext();
    }

}
