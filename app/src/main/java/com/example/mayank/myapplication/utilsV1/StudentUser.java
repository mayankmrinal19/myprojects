package com.example.mayank.myapplication.utilsV1;

/**
 * Created by mayank on 8/11/16.
 */

public class StudentUser {
    /*
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "fullname TEXT, "
                + "dob NUMERIC, "
                + "gender TEXT, "
                + "school TEXT, "
                + "grade TEXT, "
                + "thumbnail TEXT, "
                + "wallpaper TEXT, "
                + "headline TEXT, "
                + "address TEXT, "
                + "longitude NUMERIC, "
                + "latitude NUMERIC, "
                + "onboard TEXT, "
                + "userid TEXT, "
                + "invitationcode TEXT, "
     */

    private int     id;
    private String  fullname;
    private double  dob;
    private String  gender;
    private String  school;
    private String  grade;
    private String  thumbnail;
    private String  wallpaper;
    private String  headline;
    private String  address;
    private double  longitude;
    private double  latitude;
    private String  onboard;
    private String  userid;
    private String  invitationcode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public double getDob() {
        return dob;
    }

    public void setDob(double dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getWallpaper() {
        return wallpaper;
    }

    public void setWallpaper(String wallpaper) {
        this.wallpaper = wallpaper;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getOnboard() {
        return onboard;
    }

    public void setOnboard(String onboard) {
        this.onboard = onboard;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getInvitationcode() {
        return invitationcode;
    }

    public void setInvitationcode(String invitationcode) {
        this.invitationcode = invitationcode;
    }
}