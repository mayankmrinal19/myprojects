package com.example.mayank.myapplication;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;


public class PagerIndicatorView extends View implements ViewPager.OnPageChangeListener {

    private static final String TAG = PagerIndicatorView.class.getName();

    // Drawing related:
    private float circlePadding, selectedCircleRadius, unselectedCircleRadius, startEndPadding;
    private int selectedColor, unselectedColor;
    private Paint paint;

    // Other:
    private int numberOfPages, selectedPagePosition, animatedPagePosition, pageScrollState;
    private float pageScrollPositionOffset;

    public PagerIndicatorView(Context context) {
        super(context);
        initialize(null);
    }

    public PagerIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs);
    }

    public PagerIndicatorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PagerIndicatorView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int minW;
        if (numberOfPages > 0) {
            minW = (int) Math.ceil(totalWidthNeededForDots(numberOfPages));
        } else {
            minW = (int) Math.ceil(totalWidthNeededForDots(2));
        }
        int w = resolveSizeAndState(minW, widthMeasureSpec, 0);

        int minH = (int) Math.ceil(Math.max(selectedCircleRadius, unselectedCircleRadius) * 2F);
        int h = resolveSizeAndState(minH, heightMeasureSpec, 0);

        setMeasuredDimension(w, h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float height = getHeight();
        float width = getWidth();

        if (numberOfPages > 0) {
            float widthOfDots = totalWidthNeededForDots(numberOfPages);
            float startX = (width - widthOfDots) / 2F;

            float circleY = height / 2F;
            float circleX = startX + startEndPadding + unselectedCircleRadius;

            // Draw unselected circles:
            paint.setColor(unselectedColor);
            for (int i = 0; i < numberOfPages; i++) {
                canvas.drawCircle(circleX, circleY, unselectedCircleRadius, paint);
                circleX = circleX + (unselectedCircleRadius * 2F) + circlePadding;
            }

            // Draw the selected circle:
            paint.setColor(selectedColor);

            if (pageScrollState != ViewPager.SCROLL_STATE_IDLE) {
                // The user is scrolling the ViewPager:
                circleX = startX + startEndPadding + (unselectedCircleRadius * 2F * animatedPagePosition)
                        + (circlePadding * animatedPagePosition) + unselectedCircleRadius;
                float pathToNextDot = unselectedCircleRadius * 2F + circlePadding;
                circleX = circleX + pathToNextDot * pageScrollPositionOffset;
                canvas.drawCircle(circleX, circleY, selectedCircleRadius, paint);
            } else {
                // The user is NOT scrolling the ViewPager:
                circleX = startX + startEndPadding + (unselectedCircleRadius * 2F * selectedPagePosition)
                        + (circlePadding * selectedPagePosition) + unselectedCircleRadius;
                canvas.drawCircle(circleX, circleY, selectedCircleRadius, paint);
            }
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        animatedPagePosition = position;
        pageScrollPositionOffset = positionOffset;
        invalidate();
    }

    @Override
    public void onPageSelected(int position) {
        setSelected(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        pageScrollState = state;
    }

    public void setNumberOfPages(int numberOfPages) {
        boolean requestLayout = false;
        if (numberOfPages > this.numberOfPages) {
            requestLayout = true;
        }
        this.numberOfPages = numberOfPages;
        if (requestLayout) {
            requestLayout();
        }
        invalidate();
    }

    public void setSelected(int position) {
        selectedPagePosition = position;
        invalidate();
    }

    public void setViewPager(ViewPager viewPager) {
        if (viewPager == null) {
            return;
        }

        boolean requestLayout = false;
        viewPager.addOnPageChangeListener(this);
        if (viewPager.getAdapter() == null) {
            selectedPagePosition = 0;
            numberOfPages = 0;
        } else {
            selectedPagePosition = viewPager.getCurrentItem();
            numberOfPages = viewPager.getAdapter().getCount();
            requestLayout = true;
        }
        if (requestLayout) {
            requestLayout();
        }
        invalidate();
    }

    private float totalWidthNeededForDots(int numOfDots) {
        float total = numOfDots * 2F * unselectedCircleRadius;
        total = total + ((numOfDots - 1) * circlePadding) + startEndPadding;
        return total;
    }

    private void initialize(AttributeSet attrs) {
        Context context = getContext();
        circlePadding = ConversionUtils.convertSpToPixels(4.66F, context);
        selectedCircleRadius = ConversionUtils.convertSpToPixels(4F, context);
        unselectedCircleRadius = ConversionUtils.convertSpToPixels(3.66F, context);
        selectedColor = Color.WHITE;
        unselectedColor = Color.GRAY;

        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);

        if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PagerIndicatorView, 0, 0);
            try {
                circlePadding = typedArray.getDimension(R.styleable.PagerIndicatorView_piv_circlePadding, circlePadding);
                selectedCircleRadius = typedArray.getDimension(R.styleable.PagerIndicatorView_piv_selectedCircleRadius, selectedCircleRadius);
                unselectedCircleRadius = typedArray.getDimension(R.styleable.PagerIndicatorView_piv_unselectedCircleRadius, unselectedCircleRadius);

                selectedColor = typedArray.getColor(R.styleable.PagerIndicatorView_piv_selectedColor, selectedColor);
                unselectedColor = typedArray.getColor(R.styleable.PagerIndicatorView_piv_unselectedColor, unselectedColor);
            } finally {
                typedArray.recycle();
            }
        }

        startEndPadding = Math.abs(selectedCircleRadius - unselectedCircleRadius);

        numberOfPages = 0;
        selectedPagePosition = 0;
        pageScrollState = ViewPager.SCROLL_STATE_IDLE;
        pageScrollPositionOffset = 0F;

        if (isInEditMode()) {
            numberOfPages = 4;
            selectedPagePosition = 2;

//            pageScrollState = ViewPager.SCROLL_STATE_DRAGGING;
//            pageScrollPositionOffset = -0.5F;
        }
    }
}
