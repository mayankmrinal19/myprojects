package com.example.mayank.myapplication.utilsV1;

import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.text.Layout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mayank.myapplication.R;

import java.util.Random;

public class SamplePagerAdapter extends PagerAdapter {

    private final Random random = new Random();
    private int mSize;
    ImageView imageView;

    public SamplePagerAdapter() {
        mSize = 3;
    }

    public SamplePagerAdapter(int count) {
        mSize = count;
    }

    @Override public int getCount() {
        return mSize;
    }

    @Override public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override public Object instantiateItem(ViewGroup view, int position) {
//        TextView textView = new TextView(view.getContext());
//        textView.setText(String.valueOf(position + 1));
//        textView.setBackgroundColor(0xff000000 | random.nextInt(0x00f00fff));
//        textView.setGravity(Gravity.CENTER);
//        textView.setTextColor(Color.WHITE);
//        textView.setTextSize(48);

          imageView = new ImageView(view.getContext());
          imageView.setId(R.id.imageView);

//        imageView.setForegroundGravity(Gravity.CENTER);
        try {
            if (position == 2)
                imageView.setImageResource(R.drawable.black_radius);
            if (position == 1)
                imageView.setImageResource(R.drawable.white_radius);
            if (position == 0)
                imageView.setImageResource(R.drawable.img);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        view.addView(imageView, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        return imageView;
    }

    public void addItem() {
        mSize++;
        notifyDataSetChanged();
    }

    public void removeItem() {
        mSize--;
        mSize = mSize < 0 ? 0 : mSize;

        notifyDataSetChanged();
    }
}