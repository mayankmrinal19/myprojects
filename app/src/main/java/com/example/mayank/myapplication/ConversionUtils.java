package com.example.mayank.myapplication;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class ConversionUtils {

    private ConversionUtils() {

    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static float convertSpToPixels(float sp, Context context) {
        Resources resources = context.getResources();
        return sp * resources.getDisplayMetrics().scaledDensity;
    }
}
