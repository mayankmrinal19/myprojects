package com.example.mayank.myapplication.activity;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mayank.myapplication.R;
import com.example.mayank.myapplication.utilsV1.DataUtils;
import com.example.mayank.myapplication.utilsV1.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;
import com.kaopiz.kprogresshud.KProgressHUD;

public class LoginActivity extends AppCompatActivity
        implements  View.OnClickListener
{
    // Views:
    private TextView textViewSignUp, textViewLogin;
    private LoginButton fbLoginButton;

    // Dialogs:
    private KProgressHUD progressDialog;
    private AlertDialog alertDialog;

    // Other:
    private CallbackManager callbackManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize Facebook SDK:
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(LoginActivity.this, "successful", Toast.LENGTH_SHORT).show();
               startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
            }

            @Override
            public void onCancel() {
                // Do nothing.
            }

            @Override
            public void onError(FacebookException error) {
                // Do nothing.
                Toast.makeText(LoginActivity.this, "error", Toast.LENGTH_SHORT).show();
            }
        });

        setContentView(R.layout.activity_login);
        initializeViews();
    }

    @Override
    public void onStop() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.cancel();
        }
        super.onStop();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        if (view == textViewSignUp) {
//            startActivity(new Intent(this, RegistrationActivity.class));
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        } else if (view == textViewLogin) {
//            startActivity(new Intent(this, LoginEmailActivity.class));
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    private void initializeViews() {
        textViewSignUp = (TextView) findViewById(R.id.activityLogin_textViewSignUp);
        textViewLogin = (TextView) findViewById(R.id.activityLogin_textViewLogin);
        fbLoginButton = (LoginButton) findViewById(R.id.activityLogin_fbLoginButton);

        Utils.setupAnimator(textViewSignUp);
        Utils.setupAnimator(textViewLogin);
        textViewSignUp.setOnClickListener(this);
        textViewLogin.setOnClickListener(this);

        fbLoginButton.setReadPermissions("public_profile", "email");
        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                com.facebook.AccessToken.setCurrentAccessToken(loginResult.getAccessToken());
                System.out.println((loginResult.getAccessToken()));

//                progressDialog = Utils.utilKProgressHUD(LoginActivity.this);
//                progressDialog.setDetailsLabel(getString(R.string.activityLogin_fbStartToFetchDetailsMsg));
//                progressDialog.show();
//                progressDialog.show();

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                                          if (!isFinishing()) {
                                    if (object == null) {
                                        if (alertDialog != null && alertDialog.isShowing()) {
                                            alertDialog.cancel();
                                        }
                                        alertDialog = Utils.createInfoDialog(LoginActivity.this, R.string.activityLogin_fbFailedToFetchDetailsMsg);
                                        alertDialog.show();
                                    } else {
                                        try {
                                            String email = object.getString("email");
                                            String firstName = object.getString("first_name");
                                            String lastName = object.getString("last_name");
                                            StringBuilder fullNameSb = new StringBuilder(50);
                                            if (firstName != null && firstName.length() > 0) {
                                                fullNameSb.append(firstName).append(' ');
                                            }
                                            if (lastName != null && lastName.length() > 0) {
                                                fullNameSb.append(lastName);
                                            }

                                            LoginManager.getInstance().logOut();
                                            Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                                            intent.putExtra(RegistrationActivity.ONBOARD_EXTRA, DataUtils.ONBOARD_FACEBOOK);
                                            intent.putExtra(RegistrationActivity.NAME_EXTRA, fullNameSb.toString());
                                            intent.putExtra(RegistrationActivity.EMAIL_EXTRA, email);
                                            startActivity(intent);
                                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        });
                Bundle params = request.getParameters();
                params.putString("fields", "email,name,first_name,last_name");
                request.setParameters(params);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // Do nothing.
            }

            @Override
            public void onError(FacebookException exception) {
                // Do nothing.
            }
        });
    }
}
