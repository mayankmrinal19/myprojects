package com.example.mayank.myapplication.utilsV1;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

/**
 * Created by dell on 27-06-2016.
 */

public class StudentUserDBAPI {
    private static final String DB_TABLE_STUDENTUSER       = "STUDENTUSER";

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DB_TABLE_STUDENTUSER + "( "
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "fullname TEXT, "
                + "dob NUMERIC, "
                + "gender TEXT, "
                + "school TEXT, "
                + "grade TEXT, "
                + "thumbnail TEXT, "
                + "wallpaper TEXT, "
                + "headline TEXT, "
                + "address TEXT, "
                + "longitude NUMERIC, "
                + "latitude NUMERIC, "
                + "userid TEXT, "
                + "invitationcode TEXT "
                + ")");
    }

    public static StudentUser getUserProfile(SQLiteDatabase db) {
        if (db != null) {
            Cursor cursor       = null;
            try {
                cursor       = db.query(DB_TABLE_STUDENTUSER, null, null, null, null, null, null, null);

                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }

                StudentUser user   = new StudentUser();
                user.setId(cursor.getInt(0));
                user.setFullname(cursor.getString(1));
                user.setDob(cursor.getDouble(2));
                user.setGender(cursor.getString(3));
                user.setSchool(cursor.getString(4));
                user.setGrade(cursor.getString(5));
                user.setThumbnail(cursor.getString(6));
                user.setWallpaper(cursor.getString(7));
                user.setHeadline(cursor.getString(8));
                user.setAddress(cursor.getString(9));
                user.setLongitude(cursor.getDouble(10));
                user.setLatitude(cursor.getDouble(11));
                user.setUserid(cursor.getString(12));
                user.setInvitationcode(cursor.getString(13));
                cursor.close();
                return user;
            } catch (SQLiteException e) {
                if (cursor != null) {
                    cursor.close();
                }
                e.printStackTrace();
            }
        }
        return null;
    }

//    public static boolean updateUserProfile(SQLiteDatabase db, APIPutStudentProfile profile) {
//
//        removeStudentUser(db, profile.getUserid());
//
//        ContentValues values  = new ContentValues();
//        if (profile.getFullname() != null) {
//            values.put("fullname", profile.getFullname());
//        }
//        if (profile.getDob() != null ) {
//            values.put("dob", APIUtil.getMilliseconds(profile.getDob(), APIUtil.ISO8602_MILLISECONDS));
//        }
//        if (profile.getGender() != null ) {
//            values.put("gender", profile.getGender());
//        }
//        if (profile.getSchool() != null ) {
//            values.put("school", profile.getSchool());
//        }
//        if (profile.getGrade() != null ) {
//            values.put("grade", profile.getGrade());
//        }
//        if (profile.getThumbnail() != null) {
//            values.put("thumbnail", profile.getThumbnail());
//        }
//        if (profile.getWallpaper() != null) {
//            values.put("wallpaper", profile.getWallpaper());
//        }
//        if (profile.getHeadline() != null) {
//            values.put("headline", profile.getHeadline());
//        }
//        if (profile.getAddress()!= null) {
//            values.put("address", profile.getAddress());
//        }
//        if (profile.getLongitude()!= 0.0) {
//            values.put("longitude", profile.getLongitude());
//        }
//        if (profile.getLatitude()!= 0.0) {
//            values.put("latitude", profile.getLatitude());
//        }
//        if(profile.getUserid() != null){
//            values.put("userid", profile.getUserid());
//        }
//        if (db != null) {
//            try {
//                db.insert(DB_TABLE_STUDENTUSER, null, values);
//                return true;
//            } catch (SQLiteException e) {
//                e.printStackTrace();
//                return false;
//            }
//        }
//        return false;
//    }
//
//    public static boolean updateUserProfile(SQLiteDatabase db, APIGetStudentProfile profile) {
//
//        removeStudentUser(db, profile.getUserid());
//
//        ContentValues values  = new ContentValues();
//        if (profile.getFullname() != null) {
//            values.put("fullname", profile.getFullname());
//        }
//        if (profile.getDob() != null ) {
//            values.put("dob", APIUtil.getMilliseconds(profile.getDob(), APIUtil.ISO8602_MILLISECONDS));
//        }
//        if (profile.getGender() != null ) {
//            values.put("gender", profile.getGender());
//        }
//        if (profile.getSchool() != null ) {
//            values.put("school", profile.getSchool());
//        }
//        if (profile.getGrade() != null ) {
//            values.put("grade", profile.getGrade());
//        }
//        if (profile.getThumbnail() != null) {
//            values.put("thumbnail", profile.getThumbnail());
//        }
//        if (profile.getWallpaper() != null) {
//            values.put("wallpaper", profile.getWallpaper());
//        }
//        if (profile.getHeadline() != null) {
//            values.put("headline", profile.getHeadline());
//        }
//        if (profile.getAddress()!= null) {
//            values.put("address", profile.getAddress());
//        }
//        if (profile.getLongitude()!= 0.0) {
//            values.put("longitude", profile.getLongitude());
//        }
//        if (profile.getLatitude()!= 0.0) {
//            values.put("latitude", profile.getLatitude());
//        }
//        if(profile.getUserid() != null){
//            values.put("userid", profile.getUserid());
//        }
//        if (db != null) {
//            try {
//                db.insert(DB_TABLE_STUDENTUSER, null, values);
//                return true;
//            } catch (SQLiteException e) {
//                e.printStackTrace();
//                return false;
//            }
//        }
//        return false;
//    }

    private static void removeStudentUser(SQLiteDatabase db, String userid) {
        try {
            if (db != null) {
                db.delete(DB_TABLE_STUDENTUSER, "userid = ?", new String[]{userid});
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public static void updateUserInviteCode(SQLiteDatabase db, String userid, String inviteCode) {

        ContentValues values  = new ContentValues();

        if (inviteCode != null) {
            values.put("invitationcode", inviteCode);
        }

        if (db != null) {
            try {
                db.update(DB_TABLE_STUDENTUSER, values, " userid = ? ", new String[] {userid});
            } catch (SQLiteException e) {
                e.printStackTrace();
            }
        }
    }

    // remove student
    public static void removeStudent(SQLiteDatabase db) {
        try {
            if (db != null) {
                db.delete(DB_TABLE_STUDENTUSER, null, null);
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }


}
