package com.example.mayank.myapplication.utilsV1;

import android.support.design.widget.AppBarLayout;

public abstract class AppBarStateChangeListener implements AppBarLayout.OnOffsetChangedListener {

    private static final String TAG = AppBarStateChangeListener.class.getName();

    private State currentState = State.IDLE;
    private int oldVerticalOffset;

    @Override
    public final void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        try {
            if (verticalOffset == 0) {
                if (currentState != State.EXPANDED) {
                    onStateChanged(appBarLayout, State.EXPANDED);
                }
                currentState = State.EXPANDED;
            } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                if (currentState != State.COLLAPSED) {
                    onStateChanged(appBarLayout, State.COLLAPSED);
                }
                currentState = State.COLLAPSED;
            } else {
                if (currentState == State.COLLAPSED && oldVerticalOffset < verticalOffset) {
                    onStateChanged(appBarLayout, State.OPENING_IN_PROGRESS);
                    currentState = State.OPENING_IN_PROGRESS;
                } else if (currentState != State.IDLE) {
                    onStateChanged(appBarLayout, State.IDLE);
                    currentState = State.IDLE;
                }
            }
            oldVerticalOffset = verticalOffset;
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public abstract void onStateChanged(AppBarLayout appBarLayout, State state);

    public enum State {
        EXPANDED, COLLAPSED, OPENING_IN_PROGRESS, IDLE
    }
}
