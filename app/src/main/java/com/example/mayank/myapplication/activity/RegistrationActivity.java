package com.example.mayank.myapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mayank.myapplication.MyApplication;
import com.example.mayank.myapplication.R;
import com.example.mayank.myapplication.utilsV1.DataUtils;
import com.example.mayank.myapplication.utilsV1.Utils;
import com.example.mayank.myapplication.Preferences;


public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    // Startup extras:
    public static final String ONBOARD_EXTRA = RegistrationActivity.class.getName() + ".ONBOARD_EXTRA";
    public static final String EMAIL_EXTRA = RegistrationActivity.class.getName() + ".EMAIL_EXTRA";
    public static final String NAME_EXTRA = RegistrationActivity.class.getName() + ".NAME_EXTRA";

    private static final String TAG = RegistrationActivity.class.getName();

    // Views:
    private EditText editTextName, editTextEmail, editTextPassword, editTextReferralCode;
    private Button buttonRegister;

    // Dialogs:
    private ProgressDialog progressDialog;

    private AlertDialog alertDialog;

    // Other:
//    private API registerStudentApi;
    private String onboard;
    Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
//        initializeActionBar(R.id.activityRegistration_toolbar);

        initializeViews();

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        if (view == buttonRegister) {
            startActivity(new Intent(this, MainActivity.class));
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    private void initializeViews() {

        editTextName = (EditText) findViewById(R.id.activityRegistration_editTextName);
        editTextEmail = (EditText) findViewById(R.id.activityRegistration_editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.activityRegistration_editTextPassword);
        editTextReferralCode = (EditText) findViewById(R.id.activityRegistration_editTextReferralCode);
        buttonRegister = (Button) findViewById(R.id.activityRegistration_buttonRegister);

        editTextName.addTextChangedListener(updateButtonStateTextWatcher);
        editTextEmail.addTextChangedListener(updateButtonStateTextWatcher);
        editTextPassword.addTextChangedListener(updateButtonStateTextWatcher);
        buttonRegister.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        editTextName.setText(bundle.getString(NAME_EXTRA, ""));

    }

    private void onSuccessfulRegistrationHelper() {

        Preferences prefs = MyApplication.getInstance(this).getPreferences();
        prefs.setEmail(editTextEmail.getText().toString());
        prefs.setUsername(editTextName.getText().toString());
        prefs.setPassword(editTextPassword.getText().toString());
        prefs.setOnboard(onboard);
        if (DataUtils.ONBOARD_CUSTOM.equals(onboard)) {
            prefs.setVerified(false);
        } else {
            prefs.setVerified(true);
        }
        prefs.setConfirmed(false);

        if (progressDialog != null && progressDialog.isShowing()) {
            Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            progressDialog.dismiss();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
        else {
            Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }

    }

    private void updateButtonState() {
        if (editTextName.getText().length() >= 3 && editTextPassword.getText().length() >= 6
                && Utils.isValidEmailAddress(editTextEmail.getText().toString())) {
            buttonRegister.setEnabled(true);
        } else {
            buttonRegister.setEnabled(false);
        }
    }

    private TextWatcher updateButtonStateTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // Do nothing.
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // Do nothing.
        }

        @Override
        public void afterTextChanged(Editable s) {
            updateButtonState();
        }
    };
}