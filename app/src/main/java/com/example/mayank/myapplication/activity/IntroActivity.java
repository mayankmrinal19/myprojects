package com.example.mayank.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.mayank.myapplication.PagerIndicatorView;
import com.example.mayank.myapplication.R;
import com.example.mayank.myapplication.utilsV1.Utils;

import java.util.ArrayList;

public class IntroActivity extends AppCompatActivity implements View.OnClickListener {

    // Views:
    private PagerIndicatorView pagerIndicatorView;
    private ViewPager viewPager;
    private Button buttonSkip;

    // Other:
    private SectionsPagerAdapter sectionsPagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        initializeViews();
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSkip) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    private void initializeViews() {
        pagerIndicatorView = (PagerIndicatorView) findViewById(R.id.activityIntro_pagerIndicatorView);
        viewPager = (ViewPager) findViewById(R.id.activityIntro_viewPager);
        buttonSkip = (Button) findViewById(R.id.activityIntro_buttonSkip);

        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        sectionsPagerAdapter.addFragmentToList(getString(R.string.intro_header_one), getString(R.string.intro_text_one));
        sectionsPagerAdapter.addFragmentToList(getString(R.string.intro_header_two), getString(R.string.intro_text_two));
        sectionsPagerAdapter.addFragmentToList(getString(R.string.intro_header_three), getString(R.string.intro_text_three));

        viewPager.addOnPageChangeListener(opcListener);
        viewPager.setAdapter(sectionsPagerAdapter);
        pagerIndicatorView.setViewPager(viewPager);

        Utils.setupAnimator(buttonSkip);
        buttonSkip.setOnClickListener(this);
    }

    private ViewPager.OnPageChangeListener opcListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            // Do nothing.
        }

        @Override
        public void onPageSelected(int position) {
            if (position == 2) {
                buttonSkip.setText(R.string.button_done);
            } else {
                buttonSkip.setText(R.string.button_skip);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            // Do nothing.
        }
    };

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final ArrayList<Fragment> fragment_list;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            fragment_list = new ArrayList<>(3);
        }

        @Override
        public Fragment getItem(int position) {
            return fragment_list.get(position);
        }

        @Override
        public int getCount() {
            return fragment_list.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

        public void addFragmentToList(String heading, String text) {
            IntroFragment introFragment = new IntroFragment();
            Bundle b = new Bundle();
            b.putString(IntroFragment.HEADING_EXTRA, heading);
            b.putString(IntroFragment.INTRO_EXTRA, text);
            introFragment.setArguments(b);
            fragment_list.add(introFragment);
            notifyDataSetChanged();
        }
    }

    public static class IntroFragment extends Fragment {

        public static final String HEADING_EXTRA = "HEADING_EXTRA";
        public static final String INTRO_EXTRA = "INTRO_EXTRA";

        public IntroFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_intro, container, false);
            if (getArguments() != null) {
                TextView heading_tv = (TextView) rootView.findViewById(R.id.intro_tv_heading);
                TextView text_tv = (TextView) rootView.findViewById(R.id.intro_tv_text);

                heading_tv.setText(getArguments().getString(HEADING_EXTRA));
                text_tv.setText(getArguments().getString(INTRO_EXTRA));
            }
            return rootView;
        }
    }
}
