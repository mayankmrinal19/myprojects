package com.example.mayank.myapplication.utilsV1;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.example.mayank.myapplication.R;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Utils {

    private Utils() {

    }

    public static String getRealPathFromUriHelper(Context context, Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    public static File createImageFileHelper(Context context) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        try {
            return File.createTempFile("JPEG_" + timeStamp, ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static AlertDialog createInfoDialog(Activity activity, int msgResId) {
        return createInfoDialog(activity, msgResId, null);
    }

    public static AlertDialog createInfoDialog(Activity activity, int msgResId, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder adBuilder = new AlertDialog.Builder(activity);
        adBuilder.setMessage(msgResId);
        adBuilder.setCancelable(true);
        adBuilder.setNeutralButton("OK", onClickListener);
        return adBuilder.create();
    }

    public static String toCamelCase(String init) {
        if (init == null) {
            return "";
        }

        final StringBuilder ret = new StringBuilder(init.length());

        for (String word : init.split(" ")) {
            if (!word.isEmpty()) {
                ret.append(word.substring(0, 1).toUpperCase());
                ret.append(word.substring(1).toLowerCase());
            }
            if (!(ret.length() == init.length())) {
                ret.append(" ");
            }
        }
        return ret.toString();
    }

    public static boolean isValidEmailAddress(String emailStr) {
        if (emailStr == null || emailStr.length() == 0) {
            return false;
        }
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(emailStr);
        return m.matches();
    }

    public static void makeTextViewLinkClickable(TextView textView, final View.OnClickListener onClickListener) {
        if (textView == null || onClickListener == null) {
            return;
        }

        String tacText = textView.getText().toString();
        SpannableString ss = new SpannableString(tacText);
        ss.setSpan(new ClickableSpan() {

            @Override
            public void onClick(View view) {
                onClickListener.onClick(view);
            }
        }, 0, tacText.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static void setupAnimator(final View view) throws NullPointerException {
        if (view == null) throw new NullPointerException("View is null");

        final InteractiveView<View> interactiveView = new InteractiveView<View>(view);
        // not yet outside of the bounds
        interactiveView.TAG = false;

        final ValueAnimator animatorPress = ValueAnimator.ofFloat(interactiveView.getView().getScaleX(),
                interactiveView.getView().getScaleX() - 0.1F);
        animatorPress.setDuration(140l);
        animatorPress.setInterpolator(new DecelerateInterpolator());
        animatorPress.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                interactiveView.getView().setScaleY((Float) animation.getAnimatedValue());
                interactiveView.getView().setScaleX((Float) animation.getAnimatedValue());
            }
        });

        final ValueAnimator animatorUnPress = ValueAnimator.ofFloat(interactiveView.getView().getScaleX() - 0.1F,
                interactiveView.getView().getScaleX());
        animatorUnPress.setDuration(140l);
        animatorUnPress.setInterpolator(new DecelerateInterpolator());
        animatorUnPress.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                interactiveView.getView().setScaleY((Float) animation.getAnimatedValue());
                interactiveView.getView().setScaleX((Float) animation.getAnimatedValue());
            }
        });

        interactiveView.getView().setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v != interactiveView.getView())
                    return false;
                int offset = (int) ConversionUtils.convertDpToPixel(6f, v.getContext());
                Rect rect = new Rect(v.getLeft() - offset, v.getTop() - offset, v.getRight() + offset,
                        v.getBottom() + offset);

                int action = (event.getAction() & event.getActionMasked());
                if (action == MotionEvent.ACTION_DOWN) {
                    if (!animatorUnPress.isRunning() && !animatorPress.isRunning()) {
                        animatorPress.start();
                        return true;
                    }
                    return false;
                } else if (action == MotionEvent.ACTION_UP) {
                    if (!interactiveView.getTag()) {
                        animatorUnPress.start();
                        v.performClick();
                    }
                    interactiveView.setTag(Boolean.FALSE);
                    return true;
                } else if (action == MotionEvent.ACTION_MOVE) {
                    if (!rect.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())) {
                        if (!(Boolean) interactiveView.getTag() && !animatorUnPress.isRunning()) {
                            animatorUnPress.start();
                        }
                        interactiveView.setTag(Boolean.TRUE);
                    }
                    return true;
                } else if (action == MotionEvent.ACTION_CANCEL) {
                    animatorUnPress.start();
                    return true;
                }
                return false;
            }
        });
    }

    public static class InteractiveView<V extends View> {

        boolean TAG;
        V view;

        public InteractiveView(V yourView) {
            this.view = yourView;
        }

        public View getView() {
            return this.view;
        }

        boolean getTag() {
            return this.TAG;
        }

        void setTag(boolean TAG) {
            this.TAG = TAG;
        }
    }



    public static float getOrientationFromExif(String imagePath) {
        float orientation = -1;
        try {
            ExifInterface exif = new ExifInterface(imagePath);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (exifOrientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    orientation = 270;

                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    orientation = 180;

                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    orientation = 90;

                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                    orientation = 0;

                    break;
                default:
                    break;
            }
        } catch (IOException e) {
        }

        return orientation;
    }

//    public static KProgressHUD utilKProgressHUD (Context context){
//        KProgressHUD progressHUD = new KProgressHUD(context);
//        progressHUD.setAnimationSpeed(2);
//        progressHUD.setCancellable(false);
//        progressHUD.setDimAmount(.5f);
//        return progressHUD;
//    }

    public static String convertTimeToString(Timestamp date, String DateFormat) {
        String LOCAL_DATE_FORMAT = "yyyy-MM-dd HH:mm";
        String str = "";

        try {
            if(date != null) {
                SimpleDateFormat ex;
                if(DateFormat != null) {
                    ex = new SimpleDateFormat(DateFormat);
                } else {
                    ex = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                }

                str = ex.format(date);
            }
        } catch (Exception var5) {
        }

        return str;
    }

    public static long convertServerTimeToMilli(String dateStr) {
        try {
            Calendar e = Calendar.getInstance();
            TimeZone tz = e.getTimeZone();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = sdf.parse(dateStr);
            sdf.setTimeZone(tz);
            dateStr = sdf.format(date);
            date = sdf.parse(dateStr);
            return date.getTime();
        } catch (ParseException var5) {
            var5.printStackTrace();
            return 0L;
        }
    }
}
