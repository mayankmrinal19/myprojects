package com.example.mayank.myapplication.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.mayank.myapplication.R;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

public class spalsh extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(spalsh.this, IntroActivity.class));
                finish();
//                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
            }
        },2000L);

    }
}
