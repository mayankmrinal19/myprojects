package com.example.mayank.myapplication.utilsV1;

import android.text.TextUtils;

public class DataUtils {

    // Onboard constants:
    public static final String ONBOARD_FACEBOOK = "FACEBOOK";
    public static final String ONBOARD_CUSTOM = "CUSTOM";

    private DataUtils() {

    }

    public static boolean hasEnteredProfileData(StudentUser studentUser) {
        if (!TextUtils.isEmpty(studentUser.getGrade())
                && !TextUtils.isEmpty(studentUser.getFullname())
                && !TextUtils.isEmpty(studentUser.getSchool())
                && !TextUtils.isEmpty(studentUser.getOnboard())
                && !TextUtils.isEmpty(studentUser.getHeadline())) {
            return true;
        }
        return false;
    }
}
