package com.example.mayank.myapplication;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mayank on 5/11/16.
 */

public class Preferences {

    private static final String TAG = Preferences.class.getName();

    // Session-related preference keys:
    private static final String PREF_KEY_END_TIME = "PREF_KEY_END_TIME";

    // Login-related preference keys:
    private static final String PREF_KEY_PASSWORD = "PREF_KEY_PASSWORD";
    private static final String PREF_KEY_USERNAME = "PREF_KEY_USERNAME";
    private static final String PREF_KEY_EMAIL = "PREF_KEY_EMAIL";

    // Profile verification:
    private static final String PREF_KEY_IS_CONFIRMED = "PREF_KEY_IS_CONFIRMED";
    private static final String PREF_KEY_IS_VERIFIED = "PREF_KEY_IS_VERIFIED";

    // Other preference keys:
    private static final String PREF_KEY_TRING_AUTH = "PREF_KEY_TRING_AUTH";
    private static final String PREF_KEY_GCM_TOKEN = "PREF_KEY_GCM_TOKEN";
    private static final String PREF_KEY_ONBOARD = "PREF_KEY_ONBOARD";

    private static final String PREF_KEY_USERPROFILE_PASSWORD = "PREF_KEY_USERPROFILE_PASSWORD";
    private static final String PREF_KEY_USERPROFILE_USERNAME = "PREF_KEY_USERPROFILE_USERNAME";
    private static final String PREF_KEY_USERPROFILE_FULLNAME = "PREF_KEY_USERPROFILE_FULLNAME";
    private static final String PREF_KEY_USERPROFILE_RESOURCE = "PREF_KEY_USERPROFILE_RESOURCE";
    private static final String PREF_KEY_USERPROFILE_SERVER = "PREF_KEY_USERPROFILE_SERVER";
    private static final String PREF_KEY_USERPROFILE_DOMAIN = "PREF_KEY_USERPROFILE_DOMAIN";
    private static final String PREF_KEY_USERPROFILE_PORT = "PREF_KEY_USERPROFILE_PORT";

    private static final String PREF_KEY_USERPROFILE_ISLOGIN = "PREF_KEY_USERPROFILE_ISLOGIN";
    private static final String PREF_KEY_USERPROFILE_IS_SSL = "PREF_KEY_USERPROFILE_IS_SSL";

    private static final String PREF_KEY_DASHBOARD_VISIBLE      = "PREF_KEY_DASHBOARD_VISIBLE";
    private static final String PREF_KEY_ACTIVE_CONV_CONTEXT    = "PREF_KEY_ACTIVE_CONV_CONTEXT";

    private static final String PREF_KEY_NEW_JOB_LAST_PLAY_TIME = "PREF_KEY_NEW_JOB_LAST_PLAY_TIME";
    private static final String PREF_KEY_NEW_MSG_LAST_PLAY_TIME = "PREF_KEY_NEW_MSG_LAST_PLAY_TIME";

    // Name of the preferences file:
    private static final String PREFS_NAME = "tutr_prefs";

     //Other:
    private final SecureSharedPreferences encryptedPrefs;

    public boolean isLoggedIn() {
        String em = getEmail();
        String pw = getPassword();
        if (em == null || em.length() == 0 || pw == null || pw.length() == 0) {
            return false;
        }
        return true;
    }

    public boolean hasEndTimeExpired() {
        if (getEndTime() < System.currentTimeMillis()) {
            return true;
        } else {
            return false;
        }
    }



    public Preferences(Context context) {
        encryptedPrefs = new SecureSharedPreferences(context, "kSDA83jFSF73jf3saduaadfuhqwd<v(1SDa",
                PREFS_NAME);
    }

    public void clear() {
        encryptedPrefs.edit().clear().apply();
    }

    public void setGcmToken(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_GCM_TOKEN, value).apply();
    }

    public String getGcmToken() {
        return encryptedPrefs.getString(PREF_KEY_GCM_TOKEN, null);
    }



    public boolean isConfirmed() {
        return encryptedPrefs.getBoolean(PREF_KEY_IS_CONFIRMED, true);
    }

    public void setConfirmed(boolean value) {
        encryptedPrefs.edit().putBoolean(PREF_KEY_IS_CONFIRMED, value).apply();
    }

    public boolean isLogedIn() {
        return encryptedPrefs.getBoolean(PREF_KEY_USERPROFILE_ISLOGIN, true);
    }

    public void setisLogedIn(boolean value) {
        encryptedPrefs.edit().putBoolean(PREF_KEY_USERPROFILE_ISLOGIN, value).apply();
    }

    public boolean isSSL() {
        return encryptedPrefs.getBoolean(PREF_KEY_USERPROFILE_IS_SSL, true);
    }

    public void setSSL(boolean value) {
        encryptedPrefs.edit().putBoolean(PREF_KEY_USERPROFILE_IS_SSL, value).apply();
    }

    public boolean isVerified() {
        return encryptedPrefs.getBoolean(PREF_KEY_IS_VERIFIED, false);
    }

    public void setVerified(boolean value) {
        encryptedPrefs.edit().putBoolean(PREF_KEY_IS_VERIFIED, value).apply();
    }

    public void setOnboard(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_ONBOARD, value).apply();
    }

    public String getOnboard() {
        return encryptedPrefs.getString(PREF_KEY_ONBOARD, null);
    }

    public void setEmail(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_EMAIL, value).apply();
    }

    public String getEmail() {
        return encryptedPrefs.getString(PREF_KEY_EMAIL, null);
    }

    public void setUsername(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_USERNAME, value).apply();
    }

    public String getUsername() {
        return encryptedPrefs.getString(PREF_KEY_USERNAME, null);
    }

    public void setPassword(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_PASSWORD, value).apply();
    }

    public String getPassword() {
        return encryptedPrefs.getString(PREF_KEY_PASSWORD, null);
    }

    public void setEndTime(long value) {
        encryptedPrefs.edit().putLong(PREF_KEY_END_TIME, value).apply();
    }

    public long getEndTime() {
        return encryptedPrefs.getLong(PREF_KEY_END_TIME, Long.MIN_VALUE);
    }

//    public void setTringAuth(TringAuthDataStruct value) {
//        if (value == null) {
//            encryptedPrefs.edit().remove(PREF_KEY_TRING_AUTH).apply();
//        } else {
//            encryptedPrefs.edit().putString(PREF_KEY_TRING_AUTH, value.toJSON().toString()).apply();
//        }
//
//    }
//
//    public TringAuthDataStruct getTringAuth() {
//        TringAuthDataStruct ret = null;
//        String txt = encryptedPrefs.getString(PREF_KEY_TRING_AUTH, null);
//        if (txt != null && txt.length() > 0) {
//            try {
//                ret = new TringAuthDataStruct(new JSONObject(txt));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        return ret;
//    }

    public void setPrefKeyUserprofilePassword(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_USERPROFILE_PASSWORD, value).apply();
    }

    public String getPrefKeyUserprofilePassword() {
        return encryptedPrefs.getString(PREF_KEY_USERPROFILE_PASSWORD, null);
    }

    public void setPrefKeyUserprofileUsername(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_USERPROFILE_USERNAME, value).apply();
    }

    public  String getPrefKeyUserprofileUsername() {
        return encryptedPrefs.getString(PREF_KEY_USERPROFILE_USERNAME, null);
    }

    public void setPrefKeyUserprofileFullname(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_USERPROFILE_FULLNAME, value).apply();
    }

    public  String getPrefKeyUserprofileFullname() {
        return encryptedPrefs.getString(PREF_KEY_USERPROFILE_FULLNAME, null);
    }

    public void setPrefKeyUserprofileServer(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_USERPROFILE_SERVER, value).apply();
    }

    public  String getPrefKeyUserprofileServer() {
        return encryptedPrefs.getString(PREF_KEY_USERPROFILE_SERVER, null);
    }

    public void setPrefKeyUserprofileDomain(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_USERPROFILE_DOMAIN, value).apply();
    }

    public  String getPrefKeyUserprofileDomain() {
        return encryptedPrefs.getString(PREF_KEY_USERPROFILE_DOMAIN, null);
    }

    public void setPrefKeyUserprofileResource(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_USERPROFILE_RESOURCE, value).apply();
    }

    public  String getPrefKeyUserprofileResource() {
        return encryptedPrefs.getString(PREF_KEY_USERPROFILE_RESOURCE, null);
    }

    public void setPrefKeyUserprofilePort(int value) {
        encryptedPrefs.edit().putInt(PREF_KEY_USERPROFILE_PORT, value).apply();
    }

    public  int getPrefKeyUserprofilePort() {
        return encryptedPrefs.getInt(PREF_KEY_USERPROFILE_PORT, 0);
    }

    public void setPrefKeyNewJobLastPlayTime(long value) {
        encryptedPrefs.edit().putLong(PREF_KEY_NEW_JOB_LAST_PLAY_TIME, value).apply();
    }

    public long getPrefKeyNewJobLastPlayTime() {
        return encryptedPrefs.getLong(PREF_KEY_NEW_JOB_LAST_PLAY_TIME, 0);
    }

    public void setPrefKeyNewMsgLastPlayTime(long value) {
        encryptedPrefs.edit().putLong(PREF_KEY_NEW_MSG_LAST_PLAY_TIME, value).apply();
    }

    public long getPrefKeyNewMsgLastPlayTime() {
        return encryptedPrefs.getLong(PREF_KEY_NEW_MSG_LAST_PLAY_TIME, 0);
    }

    public void setPrefKeyDashboardVisible(boolean value) {
        encryptedPrefs.edit().putBoolean(PREF_KEY_DASHBOARD_VISIBLE, value).apply();
    }

    public  boolean getPrefKeyDashboardVisible() {
        return encryptedPrefs.getBoolean(PREF_KEY_DASHBOARD_VISIBLE, false);
    }

    public void setPrefKeyActiveConvContext(String value) {
        encryptedPrefs.edit().putString(PREF_KEY_ACTIVE_CONV_CONTEXT, value).apply();
    }

    public  String getPrefKeyActiveConvContext() {
        return encryptedPrefs.getString(PREF_KEY_ACTIVE_CONV_CONTEXT, null);
    }

    public SecureSharedPreferences.Editor getCustomPref(){
        return encryptedPrefs.edit();
    }

    public SecureSharedPreferences getCustomValue(){
        return encryptedPrefs;
    }
}
